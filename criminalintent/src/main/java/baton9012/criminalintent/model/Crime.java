package baton9012.criminalintent.model;

import java.util.Date;
import java.util.UUID;


//класс престуупление с полями описания
//UUID = вспомогательный класс Java, входящий в инфраструктуру Android, — предоставляет простой способ генерирования универсально-уникальных идентификаторов
public class Crime {

    private UUID mId = UUID.randomUUID();
    private String mTitle;
    private Date mDate = new Date();
    private boolean mSolved;
    private String mSuspect;

    public Crime() {
        this(UUID.randomUUID());
    }

    public Crime(UUID id){
        mId = id;
        mDate = new Date();
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setDate(Date date){
        mDate = date;
    }

    public Date getDate() {
        return mDate;
    }

    public boolean isSolved() {
        return mSolved;
    }

    public void setSolved(boolean solved) {
        mSolved = solved;
    }

    public String getSuspect() {
        return mSuspect;
    }

    public void setSuspect(String suspect) {
        mSuspect = suspect;
    }

    public String getPhotoFilename(){
        return "IMG_" + getId().toString() + ".jpg";
    }
}
