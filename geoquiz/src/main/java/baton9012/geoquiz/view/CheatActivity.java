package baton9012.geoquiz.view;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import baton9012.geoquiz.R;

public class CheatActivity extends AppCompatActivity {

    private static final String EXTRA_ANSWER_IS_TRUE = "baton9012.geoquiz.answer_is_true";
    private static final String EXTRA_ANSWER_SHOW = "baton9012.geoquiz.answer_show";
    private static final String KEY_ANSWER_IS_TRUE = "mAnswerIsTrue";

    private boolean mAnswerIsTrue;
    private boolean mIsAnswerShow;

    private TextView mAnswerTextView;
    private Button mShowAnswerButton;

    public static Intent newIntent(Context packageContext, boolean answerIsTrue){
        return new Intent(packageContext, CheatActivity.class)
                .putExtra(EXTRA_ANSWER_IS_TRUE, answerIsTrue);
    }

    public static boolean wasAnswerShow(Intent result){
        return result.getBooleanExtra(EXTRA_ANSWER_SHOW, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);
        if (savedInstanceState != null){
            mIsAnswerShow = savedInstanceState.getBoolean(KEY_ANSWER_IS_TRUE);
        }

        mAnswerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);

        mAnswerTextView = findViewById(R.id.answer_text_view);
        mShowAnswerButton = findViewById(R.id.show_answer_button);
        mShowAnswerButton.setOnClickListener(v -> {
            mAnswerTextView.setText(mAnswerIsTrue ? R.string.true_answer : R.string.false_answer);
            setAnswerShowResult(true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int cx = mShowAnswerButton.getWidth() / 2;
                int cy = mShowAnswerButton.getWidth() / 2;
                float radius = mShowAnswerButton.getWidth();
                Animator anim = ViewAnimationUtils.createCircularReveal(mShowAnswerButton, cx, cy, radius, 0);
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mShowAnswerButton.setVisibility(View.INVISIBLE);
                    }
                });
                anim.start();
            }else {
                mShowAnswerButton.setVisibility(View.INVISIBLE);
            }
        });

        TextView mVersionAndroid = findViewById(R.id.version_android);

        mVersionAndroid.setText(getString(R.string.android_version, Build.VERSION.SDK_INT));
    }

    private void setAnswerShowResult(boolean isAnswerShow){
        mIsAnswerShow = isAnswerShow;
        Intent data = new Intent()
                .putExtra(EXTRA_ANSWER_SHOW, mIsAnswerShow);
        setResult(RESULT_OK, data);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean(KEY_ANSWER_IS_TRUE, mIsAnswerShow);
    }
}
