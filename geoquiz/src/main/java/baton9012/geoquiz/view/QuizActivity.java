package baton9012.geoquiz.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import baton9012.geoquiz.R;
import baton9012.geoquiz.model.Question;

public class QuizActivity extends AppCompatActivity {

    private static final String TAG = "QuizActivity";
    private static final String KEY_INDEX = "index";
    private static final String KEY_ANSWERED_QUESTION = "mAnswered";
    private static final String KEY_ASSESSMET = "mAssessment";
    private static final String KEY_OUTPUT_ASSESMENT = "mOutputAssesment";
    private static final String KEY_IN_CHEATER = "mInCheater";
    private static final String KEY_COUNTER_CHEATS = "mCounterCheats";

    private static final int REQUEST_CODE_CHEAT = 0;

    public static final int MOVE_TO_NEXT = 1;
    public static final int MOVE_TO_PREV = -1;

    private ImageButton mPreviousButton;
    private Button mTrueButton;
    private Button mFalseButton;
    private Button mCheatButton;
    private ImageButton mNextButton;
    private TextView mQuestionTextView;

    private Question[] mQuestionBlank = new Question[] {
            new Question(R.string.question_australia, true),
            new Question(R.string.question_ocean, true),
            new Question(R.string.question_mideast, false),
            new Question(R.string.question_africa, false),
            new Question(R.string.question_americas,true),
            new Question(R.string.question_asia, true),
    };

    private boolean[] mAnswered = new boolean[mQuestionBlank.length];
    private int mCurrentIndex = 0;
    private boolean mInCheater;
    private String mOutputAssesment;
    private int mCounterCheats = 0;
    private int mAssessment = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate(Bundle) called");
        setContentView(R.layout.activity_quiz);

        if (savedInstanceState != null){
            mAnswered = savedInstanceState.getBooleanArray(KEY_ANSWERED_QUESTION);
            mAssessment = savedInstanceState.getInt(KEY_ASSESSMET);
            mOutputAssesment = savedInstanceState.getString(KEY_OUTPUT_ASSESMENT);
            mCurrentIndex = savedInstanceState.getInt(KEY_INDEX, 0);
            mInCheater = savedInstanceState.getBoolean(KEY_IN_CHEATER);
            mCounterCheats = savedInstanceState.getInt(KEY_COUNTER_CHEATS);
        }
        //поиск кнопки на view по id true_button
        mTrueButton = findViewById(R.id.true_button);
        //реакция на нажатие на кнопку
        mTrueButton.setOnClickListener(v -> {
            checkAnswer(true);
            seeResultTest();
        });

        //поиск кнопки на view по id false_button
        mFalseButton = findViewById(R.id.false_button);
        //реакция на нажатие на кнопку
        mFalseButton.setOnClickListener(view -> {
            checkAnswer(false);
            seeResultTest();
        });
        //поиск поля для ввывода вопросса
        mQuestionTextView = findViewById(R.id.question_text_view);
        mQuestionTextView.setOnClickListener(view -> {
            mCurrentIndex = (mCurrentIndex + 1) % mQuestionBlank.length;
            updateQuestion();
        });
        //поиск next_button
        mNextButton = findViewById(R.id.next_button);
        //изменение индекса списка с вопросами и изменение вопросса
        mNextButton.setOnClickListener(view -> {
            moveToQuestion(MOVE_TO_NEXT);
            mInCheater = false;
        });

        mPreviousButton = findViewById(R.id.previous_button);
        mPreviousButton.setOnClickListener(view -> {
            moveToQuestion(MOVE_TO_PREV);
        });

        mCheatButton = findViewById(R.id.cheat_button);
        mCheatButton.setOnClickListener(v -> {
            mCounterCheats = mCounterCheats+1;
            boolean answerIsTrue = mQuestionBlank[mCurrentIndex].isAnswerTrue();
            startActivityForResult(CheatActivity.newIntent(QuizActivity.this, answerIsTrue), REQUEST_CODE_CHEAT);
        });

        updateQuestion();
    }


    private void seeResultTest(){
        int counterAnsweredQuestion = 0;
        for (int i = 0; i < mQuestionBlank.length; i++){
            if (mAnswered[i]){
                counterAnsweredQuestion = counterAnsweredQuestion + 1;
            } else {
                break;
            }
        }
        if (counterAnsweredQuestion == mQuestionBlank.length){
            outputAssessment();
        }
    }

    private void moveToQuestion(int increment) {
        mCurrentIndex = (mCurrentIndex + mQuestionBlank.length + increment) % mQuestionBlank.length;
        updateQuestion();
    }


    //изменение вопросса и ввывод на экран
    private void updateQuestion() {
        int question = mQuestionBlank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);
        updateButtonsTrueFalse();
        updateButtonsNextPrevious();
        updateButtonCheat();
    }


    private void updateButtonsTrueFalse() {
        boolean answered = mAnswered[mCurrentIndex];
        mFalseButton.setEnabled(!answered);
        mTrueButton.setEnabled(!answered);
    }

    private void updateButtonsNextPrevious(){
        mNextButton.setEnabled(mCurrentIndex != mQuestionBlank.length-1);
        mPreviousButton.setEnabled(mCurrentIndex != 0);
    }

    private void updateButtonCheat (){
        mCheatButton.setEnabled(mCounterCheats != 3);
    }

    //проверка правильности ответа
    private void checkAnswer(boolean userPressedTrue) {
        boolean answerIsTrue = mQuestionBlank[mCurrentIndex].isAnswerTrue();

        int massageResId;

        if (mInCheater){
            massageResId = R.string.judgment_toast;
        }else if (userPressedTrue == answerIsTrue) {
            massageResId = R.string.correct_toast;
            mAssessment = mAssessment + 1;
        } else {
            massageResId = R.string.incorrect_toast;
        }

        //Toast всплывающий ответ(реакция на onClick, параметры: 1 контекст для идентификайии ресурса строки, 2 сама строка, продолжительность увидомления
        Toast toast = Toast.makeText(this, massageResId, Toast.LENGTH_SHORT);
        //вывод увидомления сверху поцентру параметры: 1 позиция на экране, 2 и 3 параметры для перемещения места появления сообщения
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        //ввывод
        toast.show();
        mAnswered[mCurrentIndex] = true;
        updateButtonsTrueFalse();
    }

    public void outputAssessment (){
        mQuestionTextView.setText(getString(R.string.your_assessment, mAssessment));
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode != Activity.RESULT_OK){
            return;
        }
        if (requestCode == REQUEST_CODE_CHEAT){
            if (data == null){
                return;
            }
            mInCheater = CheatActivity.wasAnswerShow(data);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart() called");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState");
        savedInstanceState.putString(KEY_OUTPUT_ASSESMENT, mOutputAssesment);
        savedInstanceState.putInt(KEY_INDEX, mCurrentIndex);
        savedInstanceState.putBooleanArray(KEY_ANSWERED_QUESTION, mAnswered);
        savedInstanceState.putInt(KEY_ASSESSMET, mAssessment);
        savedInstanceState.putBoolean(KEY_IN_CHEATER, mInCheater);
        savedInstanceState.putInt(KEY_COUNTER_CHEATS, mCounterCheats);
    }

    @Override
    public void onStop(){
        super.onStop();
        Log.d(TAG, "onStop() called");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }
}
