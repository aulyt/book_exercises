package baton9012.draganddraw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

public class BoxDrawingView extends View {

    private static final String TAG = "BoxDrawingView";
    private static final String BOXES = "boxes";

    private Box mCurrentBox;
    private List<Box> mBoxes = new ArrayList<>();
    private Paint mBoxPaint;
    private Paint mBackgroundPaint;

    //используеться при создание представления в коде
    public BoxDrawingView(Context context) {
        this(context, null);
    }

    //используеться при заполнение представления по разметки XML
    public BoxDrawingView(Context context, AttributeSet attrs){
        super(context, attrs);

        //прямоугольник рисуеться полупрозрачным красным цветом
        mBoxPaint = new Paint();
        mBoxPaint.setColor(0x22ff0000);

        //фон закрашивается серовато-белым цветом
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(0xfff8efe0);
    }

    @Override
    protected void onDraw(Canvas canvas){

        //заполнение фона
        canvas.drawPaint(mBackgroundPaint);

        for (Box box : mBoxes){
            float left = Math.min(box.getOrigin().x, box.getCurrent().x);
            float right = Math.max(box.getOrigin().x, box.getCurrent().x);
            float top = Math.min(box.getOrigin().y, box.getCurrent().y);
            float bottom = Math.max(box.getOrigin().y, box.getCurrent().y);
            canvas.drawRect(left,top,right,bottom, mBoxPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){

        PointF current = new PointF(event.getX(), event.getY());
        String action = "";

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                action = "ACTION_DOWN";
                //сброс текущего состояния
                mCurrentBox = new Box(current);
                mBoxes.add(mCurrentBox);
                break;
            case MotionEvent.ACTION_MOVE:
                action = "ACTION_MOVE";
                if (mCurrentBox != null){
                    mCurrentBox.setCurrent(current);
                    invalidate();
                }
                break;
            case MotionEvent.ACTION_UP:
                action = "ACTION_UP";
                mCurrentBox = null;
                break;
            case MotionEvent.ACTION_CANCEL:
                action = "ACTION_CANCEL";
                mCurrentBox = null;
                break;
        }

        Log.i(TAG, action + "at x=" + current.x + ", y=" + current.y);

        return true;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putParcelable("superState", super.onSaveInstanceState());
        ArrayList<Parcelable> parcelableList = new ArrayList<>();
        //parcelableList.addAll(mBoxes);
        for (Box box: mBoxes) {
            parcelableList.add(box);
        }
        bundle.putParcelableArrayList(BOXES, parcelableList);
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle){
            Bundle bundle = (Bundle) state;
            this.mBoxes = bundle.getParcelableArrayList(BOXES);
            state = bundle.getParcelable("superState");
        }
        super.onRestoreInstanceState(state);
    }
}
