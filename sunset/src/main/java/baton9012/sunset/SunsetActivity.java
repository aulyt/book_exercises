package baton9012.sunset;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

public class SunsetActivity extends SingleFragmentActivity {



    @Override
    protected Fragment createFragment() {
        return SunsetFragment.newInstence();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sunset, container, false);

        return view;
    }
}
